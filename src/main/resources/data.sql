INSERT INTO employee (id, first_name, last_name) VALUES
  (1, 'Sowmya', 'Krishna'),
  (2, 'Mridhu', 'Latha'),
  (3, 'Gowthami','Prasad'),
  (4, 'Sailu', 'Venkat'),
  (5, 'chamu', 'krishna'),
  (6, 'Rama', 'Lakshman'),
  (7, 'Saranya', 'Nataraj'),
  (8, 'Suneetha', 'Surendra');


--drop table t1;
--create table t1(id int auto_increment, first_name varchar(20), last_name varchar(20));
--insert into t1 (first_name, last_name) VALUES ('prenom','nom');
--ALTER TABLE T1 ALTER COLUMN id RESTART WITH 1;