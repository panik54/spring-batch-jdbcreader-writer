package com.sample.app.configuration;

import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;

import com.sample.app.entity.Employee;
import com.smaple.app.mappers.EmployeeRowMapper;

import org.springframework.context.annotation.PropertySource;

@PropertySource("classpath:batch.properties")
@Configuration
@EnableBatchProcessing
@EnableScheduling
public class JobConfiguration {
	@Autowired
	private JobBuilderFactory jobBuilderFactory;

	@Autowired
	private StepBuilderFactory stepBuilderFactory;

	@Autowired
	private DataSource dataSource;

	@Bean
	public JdbcCursorItemReader<Employee> jdbcCursorItemReader() {
		JdbcCursorItemReader<Employee> cursorItemReader = new JdbcCursorItemReader<>();

		cursorItemReader.setSql("SELECT id, first_name, last_name FROM employee ORDER BY first_name");
		cursorItemReader.setDataSource(dataSource);
		cursorItemReader.setRowMapper(new EmployeeRowMapper());
		return cursorItemReader;
	}

	@Bean
	public ItemWriter<? super Object> itemWriter() {
		return emps -> {
			System.out.println("\nWriting chunk to console");
			for (Object emp : emps) {
				System.out.println(emp);
			}
		};
	}

	@Bean
	public JdbcBatchItemWriter<Employee> dbItemWriter() {
		JdbcBatchItemWriter<Employee> writer = new JdbcBatchItemWriter<>();

		writer.setDataSource(dataSource);
		writer.setSql("INSERT INTO T1 (first_name, last_name) VALUES (:firstName, :lastName)");
		writer.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<Employee>());
		writer.afterPropertiesSet();

		return writer;
	}

	@Bean
	public Step stepEmptyTable() {
		return this.stepBuilderFactory.get("emptyTable T1")
				.tasklet((StepContribution contribution, ChunkContext chunkContext) -> {

					String stepName = chunkContext.getStepContext().getStepName();

					System.out.println("\t" + stepName + " executed");
					
					//new JdbcTemplate(this.dataSource).execute("TRUNCATE TABLE T1");

					return RepeatStatus.FINISHED;

				}).build();
	}


	@Bean
	public Step stepInsert() {
		return this.stepBuilderFactory.get("insertTableEmployee")
			.<Employee, Employee>chunk(3)
			.reader(jdbcCursorItemReader())
			//.writer(itemWriter())
			.writer(dbItemWriter())
			.build();
	}
		
	@Bean
	public Job myJob(JobRepository jobRepository, PlatformTransactionManager platformTransactionManager) {

		return jobBuilderFactory.get("My-First-Job")
	                .listener(jobListener())
			.start(stepEmptyTable())
			.next(stepInsert())
			.build();
	}


    private JobExecutionListener jobListener() {

        // 任务监听器1
        JobExecutionListener listener = new JobExecutionListener() {
            @Override
            public void beforeJob(JobExecution jobExecution) {
		new JdbcTemplate(dataSource).execute("TRUNCATE TABLE T1");
		new JdbcTemplate(dataSource).execute("ALTER TABLE T1 ALTER COLUMN id RESTART WITH 1");
                System.out.println("beforeJob: " + jobExecution.getJobInstance().getJobName());
            }

            @Override
            public void afterJob(JobExecution jobExecution) {
                //System.out.println("afterJob: " + jobExecution.getJobInstance().getJobName());
            }
        };

        return listener;
    }

}
